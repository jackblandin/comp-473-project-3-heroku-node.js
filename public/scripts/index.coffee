root = exports ? this

ready = () ->
	bindEvents()

bindEvents = () ->
	$("#search-submit").on "click", () ->
		search = $("#search").val()
		window.location.href = "" +
			"https://intense-woodland-7822.herokuapp.com/search?name=#{search}"
	$("#add").on "click", () ->
		name = $("#name").val()
		detail = $("#detail").val()
		address = $("#address").val()
		window.location.href = "" +
			"https://intense-woodland-7822.herokuapp.com/addFacility?"+
			"name=#{name}&detail=#{detail}&address=#{address}"

$(document).ready(ready)

