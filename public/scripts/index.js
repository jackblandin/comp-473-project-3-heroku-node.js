// Generated by CoffeeScript 1.8.0
(function() {
  var bindEvents, ready, root;

  root = typeof exports !== "undefined" && exports !== null ? exports : this;

  ready = function() {
    return bindEvents();
  };

  bindEvents = function() {
    $("#search-submit").on("click", function() {
      var search;
      search = $("#search").val();
      return window.location.href = "" + ("https://intense-woodland-7822.herokuapp.com/search?name=" + search);
    });
    return $("#add").on("click", function() {
      var address, detail, name;
      name = $("#name").val();
      detail = $("#detail").val();
      address = $("#address").val();
      return window.location.href = "" + "https://intense-woodland-7822.herokuapp.com/addFacility?" + ("name=" + name + "&detail=" + detail + "&address=" + address);
    });
  };

  $(document).ready(ready);

}).call(this);
