module.exports = function(app, parseUrlencoded) {
	
	app.get('/', function(req, res) {
  		res.sendFile(__dirName + "/public/views/index.html");
	});

	app.get('/facilities', function(req, res) {
		res.sendFile(__dirName + "/public/views/facilities.html");
	});
}